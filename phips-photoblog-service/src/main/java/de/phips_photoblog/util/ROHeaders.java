/* 
   Copyright 2019 Philipp Schuster
   
   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de 
   Twitter:  @phip1611 
 */
package de.phips_photoblog.util;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * More convenient way to access headers from a HttpServletRequest read-only.
 */
public class ROHeaders {

    private Map<String, String> headers = new HashMap<>();

    public ROHeaders(HttpServletRequest request) {
        request.getHeaderNames().asIterator().forEachRemaining(hn -> {
            headers.put(hn.toLowerCase(), request.getHeader(hn));
        });
    }

    public String getHeader(String name) {
        return headers.get(name.toLowerCase());
    }
}
