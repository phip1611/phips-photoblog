/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.controller;

import de.phips_photoblog.service.api.ImageService;
import de.phips_photoblog.service.api.dto.ImageDto;
import de.phips_photoblog.service.api.dto.ImageMetadataDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * Public Image Controller.
 *
 * @author Philipp Schuster (phip1611.de)
 */
@RestController
@RequestMapping("public")
public class PublicImageController {

    private ImageService imageService;

    @Autowired
    public PublicImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping("/image/{uuid}")
    public ResponseEntity<byte[]> createImage(@PathVariable("uuid") UUID uuid) {
        ImageDto dto = this.imageService.getImageDto(uuid);
        // tests showed that returning ResponseEntity is significantly faster than
        // returning byte[] directly or writing it into the output stream
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(dto.getMimeType()))
                .body(dto.getData());
    }

    @GetMapping("/images")
    public Page<ImageMetadataDto> getImages(Pageable pageable) {
        return this.imageService.metaFindAll(pageable);
    }
}
