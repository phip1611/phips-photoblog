/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.controller;

import de.phips_photoblog.config.auth.token.TokenAuthentication;
import de.phips_photoblog.service.api.dto.AuthTokenDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest-Controller used for creating an login and verifying the user token. Also giving the
 * current active user dto to the client.
 */
@RestController
public class AuthController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    public AuthController() {
    }

    /**
     * This endpoint is used by clients to do a login. They send a basic http auth
     * request to this service and spring security makes the magic. (see config).
     * If this method got actually invoked it means that spring security authenticated
     * the user. It returns the secret id (token) that the client needs to use for
     * all further requests.
     * @param authentication Authenticated User
     * @return token
     */
    @GetMapping(path = "login")
    public AuthTokenDto getToken(Authentication authentication) {
        if (!authentication.getClass().isAssignableFrom(TokenAuthentication.class)) {
            LOGGER.debug("Wrong Authentication Object in SecurityContextHolder");
        }
        // getPrincipal gibt nur UserDto zurück während die Details das ganze Token zurück geben
        return (AuthTokenDto) authentication.getPrincipal();
    }
}
