/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.controller.input;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserInput {
    private UUID id;

    @NotEmpty
    private String username;

    @NotNull
    private String name;

    @NotNull
    private String familyName;

    // Interface to public world
    @NotEmpty
    private String passwordPlain;

    // this field will be internally used
    // the service will set this from the plain password
    private String passwordHash;

    @NotEmpty
    private List<String> roles = new ArrayList<>();

    public UUID getId() {
        return id;
    }

    public UserInput setId(UUID id) {
        this.id = id;
        return this;
    }

    public List<String> getRoles() {
        return roles;
    }

    public UserInput setRoles(List<String> roles) {
        this.roles = roles;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserInput setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserInput setName(String name) {
        this.name = name;
        return this;
    }

    public String getFamilyName() {
        return familyName;
    }

    public UserInput setFamilyName(String familyName) {
        this.familyName = familyName;
        return this;
    }

    public String getPasswordPlain() {
        return passwordPlain;
    }

    public UserInput setPasswordPlain(String passwordPlain) {
        this.passwordPlain = passwordPlain;
        return this;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
}
