/* 
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.controller.input;

import javax.validation.constraints.NotEmpty;
import java.util.UUID;

/**
 * Class created: 2018-08-02 15:32
 *
 * @author Philipp Schuster (phip1611.de)
 */
public class ImageMetadataInput {

    private UUID id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String description;

    public UUID getId() {
        return id;
    }

    public ImageMetadataInput setId(UUID id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public ImageMetadataInput setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ImageMetadataInput setDescription(String description) {
        this.description = description;
        return this;
    }
}


