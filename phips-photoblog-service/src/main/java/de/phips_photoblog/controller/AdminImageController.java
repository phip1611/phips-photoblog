/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.controller;

import de.phips_photoblog.controller.input.ImageMetadataInput;
import de.phips_photoblog.service.api.ImageService;
import de.phips_photoblog.service.api.dto.ImageMetadataDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * Public Image Controller.
 *
 * @author Philipp Schuster (phip1611.de)
 */
@RestController
@RequestMapping(value = "admin")
public class AdminImageController {

    private ImageService imageService;

    @Autowired
    public AdminImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping()
    public List<ImageMetadataDto> getAllImagesMetadata() {
        return this.imageService.metaFindAll();
    }

    @PostMapping("/image")
    public ImageMetadataDto createImage(@RequestParam("file") MultipartFile multipartFile) throws IOException {
        return this.imageService.createImage(multipartFile);
    }

    @PostMapping("/image-metadata")
    public ImageMetadataDto updateImageMetadata(@Valid @RequestBody ImageMetadataInput input) {
        return this.imageService.updateImageMetadata(input);
    }
}
