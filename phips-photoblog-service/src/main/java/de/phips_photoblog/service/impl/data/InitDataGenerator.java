/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.impl.data;

import de.phips_photoblog.service.api.DataGenerator;
import de.phips_photoblog.service.api.UserService;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

import static java.util.stream.Collectors.toList;

@Service
public class InitDataGenerator extends DataGenerator {

    public InitDataGenerator(InitDataHolder dataHolder,
                             UserService userService) {
        super(dataHolder, userService);
    }

    @Override
    protected void generate() {
        LOGGER.info("Starting");

        // special treatment of this admin user

        var otherInitUsers = this.dataHolder.getUsers().stream()
                .filter(u -> !"admin".equals(u.getUsername()))
                .collect(toList());

        var adminUser = this.dataHolder.getUsers().stream()
                .filter(u -> "admin".equals(u.getUsername()))
                .findAny();

        adminUser.ifPresent(admin -> {
                    LOGGER.info("Admin-User found in Init-Data");
                    try {
                        this.userService.findByUsername("admin");
                        LOGGER.info("Admin-User is already in database!");
                    } catch (NoSuchElementException ex) {
                        LOGGER.info("Admin-User is not yet in database: create user!");
                        this.userService.createOrUpdateUser(admin);
                    }
        });

        otherInitUsers.forEach(userService::createOrUpdateUser);
        LOGGER.info("Created {} users!", this.dataHolder.getUsers().size());
    }
}
