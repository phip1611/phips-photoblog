/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.impl;

import de.phips_photoblog.config.YauaaConfiguration;
import de.phips_photoblog.config.auth.token.InvalidTokenAuthenticationException;
import de.phips_photoblog.domain.AuthToken;
import de.phips_photoblog.domain.User;
import de.phips_photoblog.repository.AuthTokenRepository;
import de.phips_photoblog.service.api.AuthTokenService;
import de.phips_photoblog.service.api.dto.AuthTokenDto;
import de.phips_photoblog.util.ROHeaders;
import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import static de.phips_photoblog.domain.AuthToken.LIFE_TIME;
import static java.util.stream.Collectors.toList;

@Service
public class AuthTokenServiceImpl implements AuthTokenService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthTokenServiceImpl.class);

    private final AuthTokenRepository repo;

    private final UserAgentAnalyzer userAgentAnalyzer;

    private final Base64.Encoder BASE64ENCODER = Base64.getEncoder();

    public AuthTokenServiceImpl(AuthTokenRepository repo,
                                UserAgentAnalyzer userAgentAnalyzer) {
        this.repo = repo;
        this.userAgentAnalyzer = userAgentAnalyzer;
    }

    @Override
    @Transactional
    public <T> T create(User user, ROHeaders ROHeaders, Function<AuthToken, T> mapper) {
        String hash = this.buildClientHash(ROHeaders);
        var entity = this.repo.save(new AuthToken(user, hash));
        return mapper.apply(entity);
    }

    @Override
    // without no rollback the state change by token.used() will not be commited to the DB
    @Transactional(noRollbackFor = InvalidTokenAuthenticationException.class) // not read only because properties inside the Token are refreshed
    public AuthTokenDto checkAuthentication(UUID tokenId, ROHeaders ROHeaders) throws AuthenticationException {
        var tokenOpt = repo.findById(tokenId);
        var token = tokenOpt.orElseThrow(() -> {
            LOGGER.debug("There is no token with UUID " + tokenId);
            return new InvalidTokenAuthenticationException("There is no token with UUID " + tokenId);
        });
        if (!token.isValid()) {
            token.used(false);
            LOGGER.debug("The Token is not valid!");
            throw new InvalidTokenAuthenticationException("The token is not valid!");
        }
        if (!token.getClientHash().equals(buildClientHash(ROHeaders))) {
            token.used(false);
            LOGGER.debug("The client-hash token does not match!");
            throw new InvalidTokenAuthenticationException("The client hash does not match!");
        }
        token.used(true);
        return new AuthTokenDto(token);
    }

    @Override
    public <T> T findById(UUID id, Function<AuthToken, T> mapper) {
        return repo.findById(id).map(mapper).orElseThrow();
    }

    @Override
    @Transactional
    public void deleteById(UUID id) {
        this.repo.deleteById(id);
    }

    @Override
    @Scheduled(fixedDelay = LIFE_TIME / 4, initialDelay = 60)
    @Transactional
    // TODO check if scheduler + transaction works here.. probably split scheduler and transaction into two classes
    public void invalidateOld() {
        this.repo.findAllByValidIsTrueAndCreationDateBefore(
                LocalDateTime.now().minusSeconds(LIFE_TIME)
        ).forEach(AuthToken::invalidate);
    }

    @Override
    public <T> List<T> getAll(Function<AuthToken, T> mapper) {
        return this.repo.findAll().stream().map(mapper).collect(toList());
    }

    private String buildClientHash(ROHeaders ROHeaders) {
        String userAgentHeader = ROHeaders.getHeader("user-agent");
        UserAgent userAgent = userAgentAnalyzer.parse(userAgentHeader);
        // explanation https://github.com/nielsbasjes/yauaa


        StringBuilder characteristicsBuilder = new StringBuilder();
        for (String field : YauaaConfiguration.FIELDS) {
           characteristicsBuilder.append(userAgent.getValue(field));
        }
        String characteristics = characteristicsBuilder.toString();

        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(characteristics.getBytes(StandardCharsets.UTF_8));
            return "sha256-" + BASE64ENCODER.encodeToString(hash);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.warn("No SHA-256-Algorithm found!");
            e.printStackTrace();
            return characteristics;
        }
    }
}
