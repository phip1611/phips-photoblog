/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.impl.data;

import de.phips_photoblog.controller.input.ImageMetadataInput;
import de.phips_photoblog.service.api.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@ConditionalOnProperty("generate-testdata")
public class TestImageMetadataGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestImageMetadataGenerator.class);

    private static final String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod " +
            "tempor incidunt ut labore et dolore magna aliqua.";

    private static final String LOREM_IPSUM_LONG = "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod " +
            "tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
            "ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit " +
            "esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in " +
            "culpa qui officia deserunt mollit anim id est laborum.";

    private static final String BLINDTEXT = "Ich bin Blindtext. Von Geburt an. Es hat lange gedauert, bis ich " +
            "begriffen habe, was es bedeutet, ein blinder Text zu sein: Man macht keinen Sinn.";

    private static final String BLINDTEXT_LONG = "Ich bin Blindtext. Von Geburt an. Es hat lange gedauert, bis ich " +
            "begriffen habe, was es bedeutet, ein blinder Text zu sein: Man macht keinen Sinn. Man wirkt hier und da" +
            " aus dem Zusammenhang gerissen. Oft wird man gar nicht erst gelesen. Aber bin ich deshalb ein schlechter" +
            " Text? Ich weiss, dass ich nie die Chance haben werde, im Stern zu erscheinen. Aber bin ich darum " +
            "weniger wichtig? Ich bin blind! Aber ich bin gerne Text. Und sollten Sie mich jetzt tatsächlich zu Ende " +
            "lesen, dann habe ich etwas geschafft, was den meisten \"normalen\" Texten nicht gelingt.";

    private static final List<String> titlePool = new ArrayList<>();

    static {
        titlePool.add("Donau in Budapest");
        titlePool.add("Oper in Wien");
        titlePool.add("Fernsehturm in Berlin");
        titlePool.add("Herbst im großen Garten");
    }

    private ImageService imageService;

    public TestImageMetadataGenerator(ImageService imageService) {
        this.imageService = imageService;
    }

    public void generate() {
        LOGGER.info("enriching images with test titles and descriptions");

        List<ImageMetadataInput> inputList = new ArrayList<>();
        AtomicInteger dirtyCount = new AtomicInteger();
        this.imageService.metaFindAll().forEach(image -> {
            boolean dirty = false;
            ImageMetadataInput input = new ImageMetadataInput().setId(image.getId());
            if (image.getName() == null || image.getName().isEmpty()) {
                input.setName(getRandomName());
                dirty = true;
            } else {
                input.setName(image.getName());
            }

            if (image.getDescription() == null || image.getDescription().isEmpty()) {
                input.setDescription(getRandomDescription());
                dirty = true;
            } else {
                input.setDescription(image.getDescription());
            }

            if (dirty) {
                dirtyCount.getAndIncrement();
                inputList.add(input);
            }
        });
        LOGGER.info("enriched "+dirtyCount+" images with test titles and descriptions");
        inputList.forEach(imageService::updateImageMetadata);
    }

    private String getRandomName() {
        return titlePool.get((int) (Math.random() * titlePool.size()));
    }

    private String getRandomDescription() {
        switch ((int) (Math.random() * 4)) {
            case 0: return LOREM_IPSUM;
            case 1: return LOREM_IPSUM_LONG;
            case 2: return BLINDTEXT;
            case 3:
            default: return BLINDTEXT_LONG;
        }
    }
}