/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.impl;

import de.phips_photoblog.controller.input.UserInput;
import de.phips_photoblog.domain.User;
import de.phips_photoblog.repository.UserRepository;
import de.phips_photoblog.service.api.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import static de.phips_photoblog.service.api.MapperUtils.entity;
import static java.util.stream.Collectors.toList;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository,
                           PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public <T> T createOrUpdateUser(UserInput input, Function<User, T> mapper) {
        input.setPasswordHash(this.passwordEncoder.encode(input.getPasswordPlain())); // replace plain text with hash
        if (input.getId() != null) {
            // we want to either update an existing
            var entity = this.userRepository.findById(input.getId());
            if (entity.isPresent()) {
                // we update an existing user
                return mapper.apply(entity.get().update(input));
            }
        }
        // we want to create a new user
        // if ID is set, we will use it, else we will generate one!
        var entity = this.userRepository.save(new User(input));
        return mapper.apply(entity);
    }

    @Override
    @Transactional
    public User createOrUpdateUser(UserInput input) {
        return this.createOrUpdateUser(input, entity());
    }

    @Override
    @Transactional(readOnly = true)
    public <T> List<T> getAllUsers(Function<User, T> mapper) {
        return this.userRepository.findAll().stream().map(mapper).collect(toList());
    }

    @Override
    @Transactional
    public List<User> getAllUsers() {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public <T> T findUserById(UUID id, Function<User, T> mapper) {
        return this.userRepository.findById(id).map(mapper).orElseThrow();
    }

    @Override
    @Transactional
    public User findUserById(UUID id) {
        return this.findUserById(id, entity());
    }

    @Override
    @Transactional(readOnly = true)
    public <T> T findByUsername(String username, Function<User, T> mapper) {
        return this.userRepository.findByUsername(username).map(mapper).orElseThrow();
    }

    @Override
    @Transactional
    public User findByUsername(String username) {
        return this.findByUsername(username, entity());
    }
}
