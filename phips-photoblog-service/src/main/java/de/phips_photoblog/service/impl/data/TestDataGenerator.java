/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.impl.data;

import de.phips_photoblog.service.api.DataGenerator;
import de.phips_photoblog.service.api.UserService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@ConditionalOnProperty("generate-testdata")
public class TestDataGenerator extends DataGenerator {

    public TestDataGenerator(TestDataHolder dataHolder,
                             UserService userService) {
        super(dataHolder, userService);
    }

    @Override
    protected void generate() {
        LOGGER.info("Starting");
        this.dataHolder.getUsers().forEach(user -> {
           try {
               userService.findByUsername(user.getUsername());
           } catch (NoSuchElementException ex) {
               // only create non existing
               userService.createOrUpdateUser(user);
           }
        });
        LOGGER.info("Created {} users!", this.dataHolder.getUsers().size());
    }
}
