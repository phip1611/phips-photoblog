/*
   Copyright 2019 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.impl;

import de.phips_photoblog.domain.Image;
import de.phips_photoblog.service.api.ImageCachingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Service for caching of binary image data only.
 *
 * @author Philipp Schuster (@phip1611)
 */
@Service
public class ImageCachingServiceImpl implements ImageCachingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageCachingServiceImpl.class);

    private Map<UUID, CachedImage> cache = new HashMap<>();

    private int sizeInMb = 0;

    @Override
    public Optional<byte[]> get(UUID id) {
        var cached = Optional.ofNullable(cache.get(id));
        cached.ifPresent(cachedImage -> LOGGER.debug("Returned image {} from cache", this.imgInfoPrintStr(cachedImage)));
        return cached.map(CachedImage::getData);
    }

    @Override
    public void set(Image image) {
        if (this.sizeInMb > ImageCachingService.CACHE_SIZE_LIMIT) {
            LOGGER.debug("Maximum number of cached images reached! Skip caching");
            return;
        }

        var cachedImage = new CachedImage(image);
        this.sizeInMb += toMb(cachedImage.getSize());
        LOGGER.debug("Cached image {}", this.imgInfoPrintStr(cachedImage));
        this.cache.put(image.getId(), cachedImage);
    }

    @Override
    public void remove(UUID id) {
        var cached = this.cache.get(id);
        this.sizeInMb -= toMb(cached.getSize());
        LOGGER.debug("Removed image {} from cache", this.imgInfoPrintStr(cached));
        this.cache.remove(id);
    }

    @Override
    public void free() {
        LOGGER.debug("Cleared cache");
        this.sizeInMb = 0;
        this.cache.clear();
    }

    @Override
    // every 6 hours
    @Scheduled(fixedDelay = 1000 * 60 * 60 * 6)
    public void removeOutdated() {
        LOGGER.debug("Start removing all outdated images from cache:");
        this.cache.values().stream()
                .filter(CachedImage::isInvalid)
                .map(CachedImage::getUuid)
                .forEach(this.cache::remove);
    }

    private String imgInfoPrintStr(CachedImage image) {
        return String.format("\"%s\"[%.3fMB]",
                image.getUuid(),
                image.getSize()/1024f/1024f
        );
    }

    private static class CachedImage {

        private LocalDateTime born;

        private UUID uuid;

        private byte[] data;

        CachedImage(Image image) {
            this.born = LocalDateTime.now();
            this.data = image.getData();
            this.uuid = image.getId();
        }

        boolean isInvalid() {
            var limit = this.born.plusHours(INVALIDATION_TIME_H);
            return LocalDateTime.now().isAfter(limit);
        }

        public UUID getUuid() {
            return uuid;
        }

        public int getSize() {
            return this.data.length;
        }

        public byte[] getData() {
            return data;
        }
    }

    private int toMb(int bytes) {
        return bytes / 1024 / 1024;
    }
}
