/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.impl;

import de.phips_photoblog.controller.input.ImageMetadataInput;
import de.phips_photoblog.domain.Image;
import de.phips_photoblog.repository.ImageRepository;
import de.phips_photoblog.service.api.ImageService;
import de.phips_photoblog.service.api.dto.ImageDto;
import de.phips_photoblog.service.api.dto.ImageMetadataDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Service
public class ImageServiceImpl implements ImageService {

    private ImageRepository repo;

    private ImageCachingServiceImpl cachingService;

    @Autowired
    public ImageServiceImpl(ImageRepository repo,
                            ImageCachingServiceImpl cachingService) {
        this.repo = repo;
        this.cachingService = cachingService;
    }

    @Override
    @Transactional(readOnly = true)
    public ImageDto getImageDto(UUID id) {
        return this.repo.findById(id)
                .map(image -> this.cachingService.get(id)
                    .map(cachedImage -> ImageDto.fromCached(image, cachedImage))
                    .orElseGet(() -> {
                        this.cachingService.set(image);
                        return ImageDto.fromDatabase(image);
                    })
                ).orElseThrow();
    }

    @Override
    @Transactional(readOnly = true)
    public ImageMetadataDto getImageMetadataDto(UUID id) {
        return this.repo.findById(id).map(ImageMetadataDto::new).orElseThrow(IllegalArgumentException::new);
    }

    @Override
    @Transactional
    public ImageMetadataDto updateImageMetadata(ImageMetadataInput input) {
        Optional<Image> opt = this.repo.findById(input.getId());
        // bestehende Entität updaten
        if (opt.isPresent()) {
            Image image = opt.get();
            image.update(input);
            return image.toMetadataDto();
        } else {
            throw new IllegalArgumentException("There is no image in the database with the uuid: " + input.getId());
        }
    }

    @Override
    public ImageMetadataDto createImage(MultipartFile multipartFile) throws IOException {
        if (multipartFile.isEmpty()) {
            throw new IllegalArgumentException("Multipartfile is empty!");
        }
        if (multipartFile.getOriginalFilename() == null
            || multipartFile.getOriginalFilename().isEmpty()) {
            throw new IllegalArgumentException("Multipartfile original filename is not set!");
        }
        if (multipartFile.getContentType() == null
            || multipartFile.getContentType().isEmpty()) {
            throw new IllegalArgumentException("Multipartfile original content type is not set!");
        }

        String[] split = multipartFile.getOriginalFilename().split("\\.");
        String fileExt = split[split.length - 1];
        if (!Image.AllowedType.isValid(fileExt)) {
            throw new IllegalArgumentException("Wrong file type! Allowed File types are: "
                    + Arrays.toString(Image.AllowedType.values()));
        }

        var imageEntity =  this.repo.save(new Image(multipartFile));
        this.cachingService.set(imageEntity);
        return imageEntity.toMetadataDto();
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        this.repo.deleteById(id);
        this.cachingService.remove(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ImageMetadataDto> metaFindAll() {
        return this.repo.findAllByOrderByCreationTimeDesc().stream().map(ImageMetadataDto::new).collect(toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ImageMetadataDto> metaFindAll(Pageable pageable) {
        return this.repo.findAllByOrderByCreationTimeDesc(pageable).map(ImageMetadataDto::new);
    }
}
