/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.impl.data;

import de.phips_photoblog.service.api.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@ConditionalOnProperty("generate-testdata")
public class TestImageImporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestImageImporter.class);

    private ImageService imageService;

    private TestImageMetadataGenerator testImageMetadataGenerator;

    public TestImageImporter(ImageService imageService,
                             TestImageMetadataGenerator testImageMetadataGenerator) {
        this.imageService = imageService;
        this.testImageMetadataGenerator = testImageMetadataGenerator;
        LOGGER.info("Test-Image Importer starting");
        this.importTestImages();
    }

    private void importTestImages() {
        Resource[] resources;
        try {
            resources = new PathMatchingResourcePatternResolver().getResources("testdata/images/*");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        MultipartFile multipartFile;
        String contentType;
        String split[];
        String fileExt;

        int a = 0;
        int b = 0;

        for (Resource resource : resources) {
            a++;
            split = resource.getFilename().split("\\.");
            fileExt = split[split.length - 1].toLowerCase();
            contentType = "image/"+fileExt;
            try {
                multipartFile = new MockMultipartFile(
                        resource.getFilename(),
                        resource.getFilename(),
                        contentType,
                        resource.getInputStream());
                this.imageService.createImage(multipartFile);
                b++;
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        LOGGER.info("Imported "+b+"/"+a+" images successfully.");

        this.testImageMetadataGenerator.generate();
    }
}
