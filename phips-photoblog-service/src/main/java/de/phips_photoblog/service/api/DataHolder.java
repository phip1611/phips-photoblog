/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api;

import de.phips_photoblog.controller.input.UserInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public abstract class DataHolder {

    // Non static to get the sub class
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    private List<UserInput> users = new ArrayList<>();

    public DataHolder() {
        LOGGER.info("Initialized");
    }

    public List<UserInput> getUsers() {
        return users;
    }

    public void setUsers(List<UserInput> users) {
        this.users = users;
    }
}
