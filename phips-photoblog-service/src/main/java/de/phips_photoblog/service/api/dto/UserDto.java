/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import de.phips_photoblog.domain.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class UserDto {

    private UUID id;

    private String username;

    private String name;

    private String familyName;

    @JsonIgnore
    private String passwordHash;

    private List<String> roles = new ArrayList<>();

    private UserDto() {
    }

    public UserDto(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.name = user.getName();
        this.familyName = user.getFamilyName();
        this.passwordHash = user.getPasswordHash();
        this.roles.addAll(user.getRoles());
    }

    public List<String> getRoles() {
        return roles;
    }

    public UUID getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDto userDto = (UserDto) o;
        return Objects.equals(id, userDto.id) &&
                Objects.equals(username, userDto.username) &&
                Objects.equals(name, userDto.name) &&
                Objects.equals(familyName, userDto.familyName) &&
                Objects.equals(passwordHash, userDto.passwordHash) &&
                Objects.equals(roles, userDto.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, name, familyName, passwordHash, roles);
    }
}
