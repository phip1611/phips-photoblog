/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class DataGenerator {

    protected final DataHolder dataHolder;

    protected final UserService userService;

    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public DataGenerator(DataHolder dataHolder,
                         UserService userService) {
        this.dataHolder = dataHolder;
        this.userService = userService;
        this.generate();
    }

    protected abstract void generate();
}
