/* 
   Copyright 2019 Philipp Schuster
   
   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de 
   Twitter:  @phip1611 
 */
package de.phips_photoblog.service.api;

import java.util.function.Function;

/**
 * Utils for the mapper-parameters in the services.
 *
 * @author Philipp Schuster (@phip1611)
 * @created 2019-10-05
 */
public class MapperUtils {

    /**
     * Alias of {@link Function#identity()}
     *
     * @param <E> Entity-Type
     * @return Entity-Type
     */
    public static <E> Function<E, E> entity() {
        return Function.identity();
    }

}
