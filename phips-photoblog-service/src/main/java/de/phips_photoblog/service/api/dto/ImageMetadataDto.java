/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api.dto;

import de.phips_photoblog.domain.Image;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Class created: 2018-08-02 15:32
 *
 * @author Philipp Schuster (phip1611.de)
 */
public class ImageMetadataDto {

    private UUID id;

    private String name;

    private String description;

    private String originalFilename;

    private LocalDateTime creationTime;

    private LocalDateTime lastModifiedTime;

    private int size;

    private String url;

    public ImageMetadataDto(Image image) {
        this.id               = image.getId();
        this.name             = image.getName();
        this.description      = image.getDescription();
        this.creationTime     = image.getCreationTime();
        this.lastModifiedTime = image.getLastModifiedTime();
        this.size             = image.getSize();
        this.originalFilename = image.getOriginalFilename();
        this.url = "/public/image/" + this.id.toString();
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public LocalDateTime getLastModifiedTime() {
        return lastModifiedTime;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public int getSize() {
        return size;
    }
}
