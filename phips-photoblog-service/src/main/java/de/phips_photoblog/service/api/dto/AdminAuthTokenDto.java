/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api.dto;

import de.phips_photoblog.domain.AuthToken;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * This DTO of a Auth Token holds information that only admins should see.
 */
public class AdminAuthTokenDto {

    private final UUID id;

    private final UserDto user;

    private final LocalDateTime creationDate;

    private final LocalDateTime lastUseDate;

    private final LocalDateTime lastSuccessfulUseDate;

    private final LocalDateTime invalidationDate;

    private final boolean isValid;

    public AdminAuthTokenDto(AuthToken authToken) {
        this.id = authToken.getId();
        this.user = new UserDto(authToken.getUser());
        this.creationDate = authToken.getCreationDate();
        this.lastUseDate = authToken.getLastUseDate();
        this.isValid = authToken.isValid();
        this.lastSuccessfulUseDate = authToken.getLastSuccessfulUseDate();
        this.invalidationDate = authToken.getInvalidationDate();
    }

    public UUID getId() {
        return id;
    }

    public UserDto getUser() {
        return user;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public LocalDateTime getLastUseDate() {
        return lastUseDate;
    }

    public boolean isValid() {
        return isValid;
    }

    public LocalDateTime getLastSuccessfulUseDate() {
        return lastSuccessfulUseDate;
    }

    public LocalDateTime getInvalidationDate() {
        return invalidationDate;
    }
}
