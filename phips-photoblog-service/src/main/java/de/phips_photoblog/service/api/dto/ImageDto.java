/* 
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api.dto;

import de.phips_photoblog.domain.Image;

/**
 * DTO wird nur benutzt um eine binäre Response des Bildes zu senden.
 *
 * @author Philipp Schuster (phip1611.de)
 */
public class ImageDto {

    private String mimeType;

    private byte[] data;

    private ImageDto() {
    }

    public static ImageDto fromCached(Image image, byte[] data) {
        var dto = new ImageDto();
        dto.mimeType = image.getMimeType();
        dto.data     = data;
        return dto;
    }

    public static ImageDto fromDatabase(Image image) {
        var dto = new ImageDto();
        dto.mimeType = image.getMimeType();
        dto.data     = image.getData();
        return dto;
    }

    public String getMimeType() {
        return mimeType;
    }

    public byte[] getData() {
        return data;
    }
}
