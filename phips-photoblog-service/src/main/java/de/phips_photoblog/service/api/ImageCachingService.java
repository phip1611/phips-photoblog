/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api;

import de.phips_photoblog.domain.Image;

import java.util.Optional;
import java.util.UUID;

/**
 * Service for Image-Caching.
 * This is necessary since tests showed that it takes up to 500ms for TTFB.
 */
public interface ImageCachingService {

    /**
     * How much cached filesize we allow in sum in MB.
     */
    int CACHE_SIZE_LIMIT = 100;

    /**
     * Invalidation-Time in Hours.
     */
    int INVALIDATION_TIME_H = 24;

    /**
     * Returns a cached image data if present.
     *
     * @param id ID
     * @return Imagedata if present
     */
    Optional<byte[]> get(UUID id);

    /**
     * Sets a cached image.
     *
     * @param image Image
     */
    void set(Image image);

    /**
     * Removes a image from cache.
     *
     * @param id Image UUID
     */
    void remove(UUID id);

    /**
     * Frees/Wipes the whole cache!
     */
    void free();

    /**
     * Removes outdated entries from Cache.
     */
    void removeOutdated();
}
