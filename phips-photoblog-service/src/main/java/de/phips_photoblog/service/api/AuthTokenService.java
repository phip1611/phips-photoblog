/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api;

import de.phips_photoblog.domain.AuthToken;
import de.phips_photoblog.domain.User;
import de.phips_photoblog.service.api.dto.AuthTokenDto;
import de.phips_photoblog.util.ROHeaders;
import org.springframework.security.core.AuthenticationException;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;

public interface AuthTokenService {

    /**
     * Creates a token for a user in the database and returns its ID.
     *
     * @param user User
     * @param ROHeaders Headers to make the token specific for the client details (e.g. browser, device)
     * @return The ID of the Token
     */
    <T> T create(User user, ROHeaders ROHeaders, Function<AuthToken, T> mapper);

    /**
     * Checks if a TokenAuthentication is valid. It checks if the token is expired as well as
     * the clientHash from the Request is correct.
     *
     * @param tokenId ID of the Token
     * @param ROHeaders Headers of the Request for Client Hash generation
     * @return AuthTokenDto if authentication is valid (never null, because Exceptions are thrown earlier)
     * @throws AuthenticationException if authentication is not valid
     */
    AuthTokenDto checkAuthentication(UUID tokenId, ROHeaders ROHeaders) throws AuthenticationException;

    /**
     * Finds a token by its ID.
     *
     * @param id ID
     * @param mapper Mapper-Function
     * @param <T> Return-Typ (Entity or DTO)
     * @return T
     */
    <T> T findById(UUID id, Function<AuthToken, T> mapper);

    /**
     * Deletes a Token by its ID.
     *
     * @param id ID
     */
    void deleteById(UUID id);

    /**
     * Invalidates all old tokens.
     */
    void invalidateOld();

    /**
     * Returns all Tokens mapped to T.
     *
     * @return List of all Tokens mapped to T
     */
    <T> List<T> getAll(Function<AuthToken, T> mapper);
}
