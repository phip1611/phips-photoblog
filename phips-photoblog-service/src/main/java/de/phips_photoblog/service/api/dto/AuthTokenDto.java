/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api.dto;

import de.phips_photoblog.domain.AuthToken;

import java.time.LocalDateTime;
import java.util.UUID;

public class AuthTokenDto {

    private UUID id;

    private UserDto user;

    private LocalDateTime creationDate;

    private LocalDateTime lastUseDate;

    private LocalDateTime lastSuccessfulUseDate;

    private LocalDateTime invalidationDate;

    private boolean isValid;

    private int useCount;

    private AuthTokenDto() {
    }

    public AuthTokenDto(AuthToken authToken) {
        this.id = authToken.getId();
        this.user = new UserDto(authToken.getUser());
        this.creationDate = authToken.getCreationDate();
        this.lastUseDate = authToken.getLastUseDate();
        this.isValid = authToken.isValid();
        this.lastSuccessfulUseDate = authToken.getLastSuccessfulUseDate();
        this.invalidationDate = authToken.getInvalidationDate();
        this.useCount = authToken.getUseCount();
    }

    public UUID getId() {
        return id;
    }

    public UserDto getUser() {
        return user;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public LocalDateTime getLastUseDate() {
        return lastUseDate;
    }

    public boolean isValid() {
        return isValid;
    }

    public LocalDateTime getLastSuccessfulUseDate() {
        return lastSuccessfulUseDate;
    }

    public LocalDateTime getInvalidationDate() {
        return invalidationDate;
    }

    public int getUseCount() {
        return useCount;
    }
}
