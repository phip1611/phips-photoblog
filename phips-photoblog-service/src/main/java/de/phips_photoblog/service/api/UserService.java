/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api;

import de.phips_photoblog.controller.input.UserInput;
import de.phips_photoblog.domain.User;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;

/**
 * Our own UserService that also extends the Spring Security User Details Service.
 * Necessary for authentication.
 */
public interface UserService {

    /**
     * Create or updates an user.
     *
     * @param input User Input
     * @param mapper Function to map the entity into something.
     * @param <T> Return Type
     * @return User
     */
    <T> T createOrUpdateUser(UserInput input, Function<User, T> mapper);
    /** {@link #createOrUpdateUser(UserInput, Function)} without mapping. */
    User createOrUpdateUser(UserInput input);

    /**
     * Finds all users and mapps them to the desired type.
     *
     * @param mapper Function to map the entity into something.
     * @param <T> Generic List-Type
     * @return Users
     */
    <T> List<T> getAllUsers(Function<User, T> mapper);
    /** {@link #getAllUsers(Function)} without mapping. */
    List<User> getAllUsers();

    /**
     * Finds an user by its ID.
     *
     * @param id ID
     * @param mapper Function to map the entity into something.
     * @param <T> Return Type
     * @return User
     */
    <T> T findUserById(UUID id, Function<User, T> mapper);
    /** {@link #findUserById(UUID, Function)} without mapping. */
    User findUserById(UUID id);

    /**
     * Finds an user by its Name.
     *
     * @param username Username
     * @param mapper Function to map the entity into something.
     * @param <T> Return Type
     * @return User
     */
    <T> T findByUsername(String username, Function<User, T> mapper);
    /** {@link #findByUsername(String, Function)} without mapping. */
    User findByUsername(String username);
}
