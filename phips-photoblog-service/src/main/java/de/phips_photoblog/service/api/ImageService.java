/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.service.api;

import de.phips_photoblog.controller.input.ImageMetadataInput;
import de.phips_photoblog.service.api.dto.ImageDto;
import de.phips_photoblog.service.api.dto.ImageMetadataDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * Service für Images.
 */
public interface ImageService {

    /**
     * Holt ein Bild aus der Datenbank.
     *
     * @param id
     * @return
     */
    ImageDto getImageDto(UUID id);

    /**
     * Holt die Metadaten eines Bildes aus der Datenbank.
     * @param id
     * @return
     */
    ImageMetadataDto getImageMetadataDto(UUID id);

    /**
     * Aktualisiert die Metadaten eines Bildes.
     *
     * @param input
     * @return
     */
    ImageMetadataDto updateImageMetadata(ImageMetadataInput input);

    /**
     * Erstellt ein neues Bild in der Datenbank ohne Metadaten.
     *
     * @param multipartFile
     * @return
     */
    ImageMetadataDto createImage(MultipartFile multipartFile) throws IOException;

    void delete(UUID id);

    List<ImageMetadataDto> metaFindAll();

    Page<ImageMetadataDto> metaFindAll(Pageable pageable);
}
