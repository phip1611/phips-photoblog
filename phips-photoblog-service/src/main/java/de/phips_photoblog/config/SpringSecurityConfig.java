/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.config;

import de.phips_photoblog.config.auth.basic.BasicAuthenticationProvider;
import de.phips_photoblog.config.auth.token.TokenAuthenticationProvider;
import de.phips_photoblog.config.auth.token.TokenHeaderFilter;
import de.phips_photoblog.util.ROHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationDetailsSource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;

/**
 * Configuration of Spring-Security.
 * The protection of the sensitive endpoints works like this:
 * on /login are basic-auth-requests expected for an initial log in.
 * If this is successful /login returns an UUID that the client must store.
 * <p>
 * Each request to /admin (and in future other protected endpoints) need a
 * "X-Token" Header with the UUID to be registered as authenticated.
 */
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    private TokenAuthenticationProvider tokenAuthenticationProvider;
    private BasicAuthenticationProvider basicAuthenticationProvider;
    private TokenHeaderFilter tokenHeaderFilter;

    @Autowired
    public SpringSecurityConfig(BasicAuthenticationProvider basicAuthenticationProvider,
                                TokenAuthenticationProvider tokenAuthenticationProvider,
                                TokenHeaderFilter tokenHeaderFilter) {
        this.tokenAuthenticationProvider = tokenAuthenticationProvider;
        this.basicAuthenticationProvider = basicAuthenticationProvider;
        this.tokenHeaderFilter = tokenHeaderFilter;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        // for initial login (and giving users their token
        auth.authenticationProvider(this.basicAuthenticationProvider);
        // // for remember logged in users / validating the token
        auth.authenticationProvider(this.tokenAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // cors should only apply if the CorsWebConfig is provided, therefore we need no switch here!
        http.cors().and()
                // probably we don't need csrf anyway because the attacker never has the token!
                .csrf().disable()
                .headers().cacheControl().disable().and() // we set cache control in our reverse proxy!!
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

                .addFilterBefore(this.tokenHeaderFilter, BasicAuthenticationFilter.class)
                // Don't fucking add 'ROLE_' here.. it crashes the fuck everything
                .authorizeRequests().antMatchers("/admin**").hasRole("ADMIN").and()
                .authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/login").permitAll().and()
                .authorizeRequests().antMatchers("/login").authenticated().and()
                // httpBasic() adds a BasicAuthenticationFilter to the chain that is creating a
                // UsernamePasswordAuthenticationToken that I do provide an AuthenticationProvider for

                // we add a detailssource-configuration so that we can access the user agent in the authentication
                // to get characteristics from it for the authentication
                .httpBasic()
                    .authenticationDetailsSource(headersDetailsSource())
                    /*
                    Tests showed that this works on the one hand but it somehow duplicates
                    the vary-headers (not sure with this!). so it is easier to use let the
                    frontend just send a "X-Requested-With: XmlHttpRequest"-Header
                    .authenticationEntryPoint((request, response, authException) -> {
                        // prevent "WWW-Authenticate: Basic"-Header which let
                        // the browser display an Login-Dialog
                        response.setHeader("WWW-Authenticate", "FormBased");
                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.getMessage());
                    })*/;
    }


    /**
     * We want the headers as additional details in the authentication because we want to add client details like
     * browser into the authentication.
     *
     * @return Headers wrapped inside AuthenticationDetailSource
     */
    private AuthenticationDetailsSource<HttpServletRequest, ROHeaders> headersDetailsSource() {
        return ROHeaders::new;
    }


}
