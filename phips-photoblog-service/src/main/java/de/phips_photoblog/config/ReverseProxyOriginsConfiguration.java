/*
   Copyright 2019 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

/**
 * Configures allowed origins in reverse proxy scenarios.
 */
@Configuration
@Profile("behind-reverse-proxy")
public class ReverseProxyOriginsConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReverseProxyOriginsConfiguration.class);

    /**
     * The Host, e.g. "blog.domain.tld".
     */
    @Value("#{environment.HOST}")
    private String host;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        String[] coorsHostsArray = new String[] {"https://" + host, "https://api." + host};
        LOGGER.info("configuring allowed origins (cors) to host: {}", Arrays.toString(coorsHostsArray));
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins(coorsHostsArray)
                        .exposedHeaders("location");
            }
        };
    }
}
