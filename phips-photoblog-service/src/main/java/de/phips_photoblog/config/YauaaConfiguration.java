/* 
   Copyright 2019 Philipp Schuster
   
   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de 
   Twitter:  @phip1611 
 */
package de.phips_photoblog.config;

import nl.basjes.parse.useragent.UserAgent;
import nl.basjes.parse.useragent.UserAgentAnalyzer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class YauaaConfiguration {

    public static final String[] FIELDS = {
            UserAgent.DEVICE_CLASS,
            UserAgent.DEVICE_BRAND,
            UserAgent.OPERATING_SYSTEM_CLASS,
            UserAgent.OPERATING_SYSTEM_NAME,
            UserAgent.OPERATING_SYSTEM_VERSION,
            UserAgent.AGENT_CLASS,
            UserAgent.AGENT_NAME // Browser Name
    };

    /**
     * Creates a Bean of Yauaa. Invokes "preheat" and sets only the fields we need for maximum performance during
     * runtime.
     *
     * @return UserAgentAnalyzer
     */
    @Bean
    public UserAgentAnalyzer userAgentAnalyzer() {
        return UserAgentAnalyzer
                .newBuilder()
                .withFields(FIELDS)
                .hideMatcherLoadStats()
                .withCache(10000)
                .preheat()
                .build();
    }

}


/*
        String characteristics =  userAgent.getValue(UserAgent.DEVICE_CLASS)
                + userAgent.getValue(UserAgent.DEVICE_NAME)
                + userAgent.getValue(UserAgent.DEVICE_BRAND)
                + userAgent.getValue(UserAgent.OPERATING_SYSTEM_CLASS)
                + userAgent.getValue(UserAgent.OPERATING_SYSTEM_NAME)
                + userAgent.getValue(UserAgent.OPERATING_SYSTEM_VERSION)
                + userAgent.getValue(UserAgent.AGENT_CLASS)
                + userAgent.getValue(UserAgent.AGENT_NAME);
 */