/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.config.auth.token;

import de.phips_photoblog.service.api.dto.AuthTokenDto;
import de.phips_photoblog.util.ROHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.UUID;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/**
 * Token-Authentication that holds a Token Entity DTO. The token know its user and so its roles/authorities.
 * Each request to secured endpoints has a "X-Token"-Header. The header holds a ID of a token and the associated
 * Token will be stored in this authentication as DTO.
 *
 * This class is instantiated by the TokenFilter and will be introduced to spring
 * security as active Authentication.
 *
 * Implementations which use this class should be immutable. (Spring Doc)
 */
public class TokenAuthentication extends AbstractAuthenticationToken {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenAuthentication.class);

    /**
     * The Token-ID from the Request in the X-Token-Header. It needs to get verified.
     *
     * Either this is nut null or {@link #authToken}.
     */
    private final UUID tokenId;

    /**
     * The fully authenticated AuthToken-entity with its user.
     *
     * Either this is nut null or {@link #tokenId}.
     */
    private final AuthTokenDto authToken;


    /**
     * The value that {@link #getDetails()} will return in the case that {@link #isAuthenticated()} is false.
     * The token is only valid for specific client characteristics obtained from the headers.
     */
    private final ROHeaders ROHeaders;

    /**
     * Constructor for a fully authenticated authentication.
     *
     * @param authToken valid AuthTokenDto
     */
    public TokenAuthentication(AuthTokenDto authToken) {
        super(authToken.getUser().getRoles().stream().map(SimpleGrantedAuthority::new).collect(toList()));
        this.authToken = authToken;
        this.setAuthenticated(true);
        this.tokenId = null;
        this.ROHeaders = null;
        LOGGER.debug("New TokenAuthentication-Object created from AuthTokenDto: authenticated=true");
    }

    /**
     * Constructor when the token id is known but we don't know yet if it is valid. In this case this
     * TokenAuthentication will be delegated to the TokenAuthenticationProvider for further checks.
     *
     * @param tokenId ID of the token to be verified (X-Token-Header-value)
     * @param ROHeaders Headers to check the client of the token
     */
    public TokenAuthentication(UUID tokenId, ROHeaders ROHeaders) {
        super(emptyList());
        this.tokenId = tokenId;
        this.authToken = null;
        this.ROHeaders = ROHeaders;
        LOGGER.debug("New TokenAuthentication-Object created from Token-ID in X-Token-Header: authenticated=false (noch)");
        // this.setAuthenticated(false); // this.setAuthenticated(false); default value from super class
        // because this is set to false an AuthProvider known to spring security
        // will verify this authentication object!
    }

    @Override
    /**
     * @return UUID of authenticated is false or AuthTokenDto if authenticated is true.
     */
    public UUID getCredentials() {
        return (authToken == null) ? tokenId : this.authToken.getId();
    }

    @Override
    public AuthTokenDto getPrincipal() {
        return this.authToken;
    }

    @Override
    public String getName() {
        return this.authToken.getUser().getUsername();
    }

    @Override
    public Object getDetails() {
        return this.ROHeaders;
    }
}