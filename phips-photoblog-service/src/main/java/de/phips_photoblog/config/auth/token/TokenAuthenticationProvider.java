/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.config.auth.token;

import de.phips_photoblog.service.api.AuthTokenService;
import de.phips_photoblog.service.api.dto.AuthTokenDto;
import de.phips_photoblog.util.ROHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * This class proves if a request with the "X-Token" header is a authenticated one
 * by checking the token.
 */
@Component
public class TokenAuthenticationProvider implements AuthenticationProvider {

    private AuthTokenService authTokenService;

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenAuthentication.class);

    public TokenAuthenticationProvider(AuthTokenService authTokenService) {
        this.authTokenService = authTokenService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.debug("validating token authentication");

        TokenAuthentication tokenAuthentication = (TokenAuthentication) authentication;

        if (authentication.isAuthenticated()) {
            // authentication provider should only be called when isAuthenticated is false
            LOGGER.error("Authentication is already authenticated! Here be dragons?!");
        }

        UUID authTokenId = tokenAuthentication.getCredentials();
        ROHeaders ROHeaders = (ROHeaders) tokenAuthentication.getDetails();
        // not null, because there would be a authentication exception here
        AuthTokenDto authToken = this.authTokenService.checkAuthentication(authTokenId, ROHeaders);


        LOGGER.debug("token is valid");

        // creating a new instance to fulfill immutable contract for Authentication Objects
        return new TokenAuthentication(authToken);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return TokenAuthentication.class.isAssignableFrom(authentication);
    }
}
