/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.config.auth.token;

import de.phips_photoblog.domain.AuthToken;
import de.phips_photoblog.util.ROHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * This filter ensures that we can validate requests against protected endpoints for the
 * "X-Token"-Header. The Value of the Header is the id of a {@link AuthToken}
 */
@Component
public class TokenHeaderFilter extends OncePerRequestFilter {

    public final static String TOKEN_HEADER_NAME = "X-Token";

    private final static Logger LOGGER = LoggerFactory.getLogger(TokenHeaderFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String tokenString = request.getHeader(TOKEN_HEADER_NAME);

        // only apply for requests where X-Token is set
        if (tokenString != null) { // (it's a check if the header is present)
            LOGGER.debug("retrieve token from request");
            UUID tokenId = UUID.fromString(tokenString);

            // To check the authentication we need the token id as well as the request headers
            // this is the recommended spring way to set additional data in the details-field of a Authentication
            SecurityContextHolder.getContext().setAuthentication(new TokenAuthentication(tokenId, new ROHeaders(request)));
        }

        filterChain.doFilter(request, response);

    }
}