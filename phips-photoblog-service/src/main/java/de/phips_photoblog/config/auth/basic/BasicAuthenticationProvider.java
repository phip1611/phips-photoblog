/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.config.auth.basic;

import de.phips_photoblog.config.auth.token.TokenAuthentication;
import de.phips_photoblog.service.api.AuthTokenService;
import de.phips_photoblog.service.api.UserService;
import de.phips_photoblog.service.api.dto.AuthTokenDto;
import de.phips_photoblog.util.ROHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import static de.phips_photoblog.service.api.MapperUtils.entity;

/**
 * This BasicAuthenticationProvider is authenticating an UsernamePasswordAuthenticationToken when
 * ths user does a http basic auth login to the /login endpoint.
 * Because we use .httpBasic() in Spring Security Config a BasicHttpAuthFilter is added to the chain.
 * This adds a UsernamePasswordAuthenticationToken to the Spring Security Context. The Spring Secruity
 * Context knows about this class because it is a @Component.
 */
@Component
public class BasicAuthenticationProvider implements AuthenticationProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(BasicAuthenticationProvider.class);

    private final UserService userService;

    private final AuthTokenService authTokenService;

    private final PasswordEncoder passwordEncoder;

    public BasicAuthenticationProvider(UserService userService,
                                       AuthTokenService authTokenService,
                                       PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.authTokenService = authTokenService;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * If there is a UsernamePasswordAuthenticationToken-Request (via HttpBasic) this AuthenticationProvider
     * takes the request and checks the username and password. If everything is correct, it returns
     * an properly authenticated {@link TokenAuthentication}.
     *
     * @param authentication UsernamePasswordAuthenticationToken from Basic Auth
     * @return fulyl authenticated TokenAuthentication
     * @throws AuthenticationException if something is wrong
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.debug("Basic-Auth-Login: Verifizieren");

        String username = authentication.getName();
        String password = (String) authentication.getCredentials();

        // no such username
        var user = this.userService.findByUsername(username, entity());

        // wrong password, hash does not match
        boolean isAuthenticated = passwordEncoder.matches(password, user.getPasswordHash());
        if (!isAuthenticated) {
            LOGGER.debug("The password does not match!");
            throw new BadCredentialsException("The password does not match!");
        }

        AuthTokenDto dto = this.authTokenService.create(user, (ROHeaders) authentication.getDetails(), AuthTokenDto::new);

        LOGGER.debug("Basic-Auth-Login erfolgreich: Authentication im SecurityContextHolder durch TokenAuthentication ersetzen");
        return new TokenAuthentication(dto);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        // because Base64 is a UsernamePassword Authentication
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
