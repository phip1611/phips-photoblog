/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.repository;

import de.phips_photoblog.domain.AuthToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AuthTokenRepository extends JpaRepository<AuthToken, UUID> {

    Optional<AuthToken> findOneById(UUID secretId);

    /**
     * Find all valid tokens that are beyond the specified expire date
     * @param time
     * @return
     */
    List<AuthToken> findAllByValidIsTrueAndCreationDateBefore(LocalDateTime time);
}
