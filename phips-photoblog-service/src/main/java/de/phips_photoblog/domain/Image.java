/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.domain;

import de.phips_photoblog.controller.input.ImageMetadataInput;
import de.phips_photoblog.service.api.dto.ImageMetadataDto;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Ein Image repräsentiert ein Bild in der Datenbank. Das Bild umfasst neben der Binär-
 * daten zahlreiche Metadaten.
 *
 * Das Erstellen eines Images in der Datenbank erfolgt in zwei Schritten bzw in zwei Requests.
 * Im UI sieht es für den Nutzer aus wie ein Formular, was aber passierten muss ist folgendes:
 *  - zuerst wird das Multipartfile an das Backend gesendet und das Bild in die DB gespeichert
 *  - danach kommt bei Erfolg eine UUID zurück, die dann für den Request mit den Metadaten
 *    genutzt wird.
 *
 * @author Philipp Schuster (phip1611.de)
 */
@Entity
public class Image {

    @Id
    private UUID id;

    private String name;

    @Column(columnDefinition = "VARCHAR(2000)")
    private String description;

    private String originalFilename;

    private String mimeType;

    private LocalDateTime creationTime;

    private LocalDateTime lastModifiedTime;

    @OneToOne(orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private BinaryData binaryData;

    private int size;

    // default konstruktor für hibernate
    private Image() {
    }

    public Image(MultipartFile multipartFile) throws IOException {
        this.id               = UUID.randomUUID();
        this.creationTime     = LocalDateTime.now();
        this.lastModifiedTime = LocalDateTime.now();
        this.mimeType         = multipartFile.getContentType();
        this.originalFilename = multipartFile.getOriginalFilename();
        this.size             = multipartFile.getBytes().length;
        this.binaryData       = new BinaryData(multipartFile.getBytes());
    }

    public void update(ImageMetadataInput input) {
        this.lastModifiedTime = LocalDateTime.now();
        this.name             = input.getName();
        this.description      = input.getDescription();
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public LocalDateTime getLastModifiedTime() {
        return lastModifiedTime;
    }

    public byte[] getData() {
        return binaryData.getData();
    }

    public String getMimeType() {
        return mimeType;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public int getSize() {
        return size;
    }

    public ImageMetadataDto toMetadataDto() {
        return new ImageMetadataDto(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return size == image.size &&
                Objects.equals(name, image.name) &&
                Objects.equals(description, image.description) &&
                Objects.equals(originalFilename, image.originalFilename) &&
                Objects.equals(mimeType, image.mimeType) &&
                Objects.equals(creationTime, image.creationTime) &&
                Objects.equals(lastModifiedTime, image.lastModifiedTime) &&
                Objects.equals(binaryData, image.binaryData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, originalFilename, mimeType, creationTime, lastModifiedTime, binaryData, size);
    }

    public enum AllowedType {
        JPEG,
        JPG,
        PNG;

        public static boolean isValid(String fileExt) {
            return List.of(AllowedType.values()).stream()
                    .map(Enum::name)
                    .map(String::toLowerCase)
                    .anyMatch(x -> x.equals(fileExt.toLowerCase()));
        }
    }
}
