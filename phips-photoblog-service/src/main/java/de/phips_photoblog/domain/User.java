/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.domain;

import de.phips_photoblog.controller.input.UserInput;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * A token is granted to a user once he signed in to remember his authentication.
 */
@Entity
@Table(name = "app_user") // "user" is a reserved keyword in postgres
public class User {

    @Id
    private UUID id;

    @Column(unique = true)
    private String username;

    private String name;

    private String familyName;

    private String passwordHash;

    @ElementCollection(targetClass = String.class)
    private List<String> roles = new ArrayList<>();

    private User() {
    }

    public User(UserInput input) {
        this.id = input.getId() != null ? input.getId() : UUID.randomUUID();
        this.update(input);
    }

    public User update(UserInput input) {
        this.username = input.getUsername();
        this.name = input.getName();
        this.familyName = input.getFamilyName();
        this.passwordHash = input.getPasswordHash();
        this.roles.retainAll(input.getRoles());
        this.roles.addAll(input.getRoles());
        return this;
    }

    public UUID getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public List<String> getRoles() {
        return this.roles;
    }
}
