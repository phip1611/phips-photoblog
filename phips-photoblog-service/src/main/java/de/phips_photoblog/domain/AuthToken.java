/*
   Copyright 2018 Philipp Schuster

   Web:      phip1611.de
   E-Mail:   philipp.schuster@phip1611.de
   Twitter:  @phip1611
 */
package de.phips_photoblog.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * An AuthToken represents an authenticated user/session. To have an ongoing "session"-like experience
 * a user shall send this token to secured endpoints instead of Basic Auth so that the password cannot
 * be stolen. To make it harder to steal the token additional client-info like the Browser Name will be
 * stored.
 */
@Entity
public class AuthToken {

    public static final int LIFE_TIME = 60 * 60; // 1 hour

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthToken.class);

    /**
     * The ID that will be stored at the client for further authentication.
     */
    @Id
    private UUID id;

    /**
     * The associated User of this token.
     */
    @JoinColumn(updatable = false, referencedColumnName = "id")
    @ManyToOne
    private User user;

    /**
     * Use-counter.
     */
    private int useCount;

    /**
     * The creation date of the token.
     */
    @Column(updatable = false)
    private LocalDateTime creationDate;

    /**
     * The last use date of the token.
     */
    private LocalDateTime lastUseDate;

    /**
     * The last successful use date of the token.
     */
    private LocalDateTime lastSuccessfulUseDate;

    /**
     * The date this token became invalid.
     */
    @Column(updatable = false)
    private LocalDateTime invalidationDate;

    /**
     * A SHA-256-Hash representing the client.
     */
    @Column(updatable = false)
    private String clientHash;

    /**
     * Whether the token is valid (e.g. not expired) or not.
     */
    private boolean valid;

    // Hibernate constructor
    private AuthToken() {
    }

    public AuthToken(User user, String clientHash) {
        this.id            = UUID.randomUUID();
        this.useCount      = 1;
        this.valid         = true;
        this.user          = user;
        this.creationDate  = LocalDateTime.now();
        this.clientHash    = clientHash;
        this.invalidationDate = null;  // token was never used at this time
        this.lastUseDate      = null;  // token was never used at this time
        this.lastSuccessfulUseDate = null;  // token was never used at this time
    }

    /**
     * Notify a token that is was used for a authentication. Updates internal state.
     *
     * @param success Successfull authentication or not
     */
    public void used(boolean success) {
        this.useCount++;
        this.lastUseDate = LocalDateTime.now();
        if (success) {
            this.lastSuccessfulUseDate = LocalDateTime.now();
        }
    }

    public void invalidate() {
        this.valid = false;
        this.invalidationDate = LocalDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public LocalDateTime getLastUseDate() {
        return lastUseDate;
    }

    public String getClientHash() {
        return clientHash;
    }

    public boolean isValid() {
        return valid;
    }

    public LocalDateTime getLastSuccessfulUseDate() {
        return lastSuccessfulUseDate;
    }

    public LocalDateTime getInvalidationDate() {
        return invalidationDate;
    }

    public int getUseCount() {
        return useCount;
    }
}
