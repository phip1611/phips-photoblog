package de.phips_photoblog;

import de.phips_photoblog.controller.input.UserInput;
import de.phips_photoblog.service.api.AuthTokenService;
import de.phips_photoblog.service.api.UserService;
import de.phips_photoblog.service.api.dto.AuthTokenDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static de.phips_photoblog.config.auth.token.TokenHeaderFilter.TOKEN_HEADER_NAME;
import static de.phips_photoblog.service.api.MapperUtils.entity;
import static java.util.Collections.singletonList;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class PhipsPhotoblogApplicationTest {

    private static UserInput testAdminUserInput;

    // we get our custom bean (see RestTemplateConfiguration)
    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthTokenService tokenService;

    @BeforeClass
    public static void setUp() {
        UUID id = UUID.randomUUID();
        testAdminUserInput = new UserInput()
                .setUsername("admin-" + id)
                .setName("Großer Admin")
                .setFamilyName("von und zu Gottes Gnaden")
                .setRoles(singletonList("ROLE_ADMIN"))
                .setId(id)
                .setPasswordPlain("password");
    }

    @Before
    public void beforeTestCreateOrUpdateUser() {
        userService.createOrUpdateUser(testAdminUserInput);
    }

    @Test()
    public void testAuthenticationBasicAndToken() {
        var res = loginWithBasicAuth();
        Assert.assertTrue(res.getStatusCode().is2xxSuccessful());
        AuthTokenDto at = res.getBody();
        Assert.assertNotNull(at);

        var res2 = loginWithToken(at.getId());
        Assert.assertTrue(res2.getStatusCode().is2xxSuccessful());
        AuthTokenDto at2 = res.getBody();
        Assert.assertNotNull(at2);

        Assert.assertEquals(at.getId(), at2.getId());
        Assert.assertEquals(at.getUser(), at2.getUser());

        var res3 = loginWithTokenAsOtherClient(at.getId());
        Assert.assertTrue(res3.getStatusCode().is4xxClientError());

        var entity = tokenService.findById(at.getId(), entity());
        Assert.assertEquals(3, entity.getUseCount());
        Assert.assertNotEquals(entity.getLastSuccessfulUseDate(), entity.getLastUseDate());
    }

    private ResponseEntity<AuthTokenDto> loginWithBasicAuth() {
        TestRestTemplate adminBasicAuthTemplate = restTemplate.withBasicAuth(testAdminUserInput.getUsername(), testAdminUserInput.getPasswordPlain());
        return adminBasicAuthTemplate.getForEntity("/login", AuthTokenDto.class);
    }

    private ResponseEntity<AuthTokenDto> loginWithToken(UUID tokenId) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(TOKEN_HEADER_NAME, tokenId.toString());
        HttpEntity<Void> tokenEntity = new HttpEntity<>(httpHeaders);
        return restTemplate.exchange("/login", HttpMethod.GET, tokenEntity, AuthTokenDto.class);
    }

    // test to prevent easy session stealing
    private ResponseEntity<AuthTokenDto> loginWithTokenAsOtherClient(UUID tokenId) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(TOKEN_HEADER_NAME, tokenId.toString());
        httpHeaders.set("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36");
        HttpEntity<Void> tokenEntity = new HttpEntity<>(httpHeaders);
        return restTemplate.exchange("/login", HttpMethod.GET, tokenEntity, AuthTokenDto.class);
    }
}
