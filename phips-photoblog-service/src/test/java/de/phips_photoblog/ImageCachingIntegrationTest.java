package de.phips_photoblog;

import de.phips_photoblog.service.api.ImageCachingService;
import de.phips_photoblog.service.api.ImageService;
import de.phips_photoblog.service.api.dto.ImageDto;
import de.phips_photoblog.service.api.dto.ImageMetadataDto;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles({"test", "testdata"}) // testdata profil for images in Database
@DirtiesContext
public class ImageCachingIntegrationTest {

    @Autowired
    private ImageCachingService imageCachingService;

    @Autowired
    private ImageService imageService;

    @Test
    public void testImageCacheAcceleration() {
        // Cache is already "full" because the test data generation
        // added each image to the cache!
        this.imageCachingService.free();

        var allImagesMeta = this.imageService.metaFindAll();
        var begin_uncached = LocalDateTime.now();
        this.getAllImagesWithBinaryData(allImagesMeta);
        var end_uncached = LocalDateTime.now();

        var begin_cached1 = LocalDateTime.now();
        this.getAllImagesWithBinaryData(allImagesMeta);
        var end_cached1 = LocalDateTime.now();

        var begin_cached2 = LocalDateTime.now();
        this.getAllImagesWithBinaryData(allImagesMeta);
        var end_cached2 = LocalDateTime.now();

        var uncached_duration_ms = Duration.between(begin_uncached, end_uncached).toMillis();
        var cached1_duration_ms = Duration.between(begin_cached1, end_cached1).toMillis();
        var cached2_duration_ms = Duration.between(begin_cached2, end_cached2).toMillis();

        Assert.assertTrue(uncached_duration_ms > cached1_duration_ms);
        Assert.assertTrue(uncached_duration_ms > cached2_duration_ms);
    }

    private List<ImageDto> getAllImagesWithBinaryData(List<ImageMetadataDto> images) {
        return images.stream()
                .map(ImageMetadataDto::getId)
                .map(this.imageService::getImageDto)
                .collect(Collectors.toList());
    }

}
