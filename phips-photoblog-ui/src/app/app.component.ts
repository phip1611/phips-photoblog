import { Component, OnInit } from '@angular/core';
import { AuthService } from './phips-photoblog-common/service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  constructor(public authService: AuthService) {
  }

  logoutHandler() {
    this.authService.logout();
  }

  ngOnInit(): void {
    // this can not be called in the constructor because otherwise
    // there are hundreds of hundreds of calls (cyclic thing)..
    this.authService.testAuthenticationOnBootstrap();
  }
}
