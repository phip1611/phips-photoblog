import { NgModule } from '@angular/core';
import { PhipsPhotoblogCommonModule } from '../phips-photoblog-common/phips-photoblog-common.module';
import { AdminHomeComponent } from './admin-home/admin-home.component';

@NgModule({
  imports: [
    PhipsPhotoblogCommonModule
  ],
  declarations: [AdminHomeComponent]
})
export class PhipsPhotoblogAdminModule { }
