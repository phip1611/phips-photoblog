import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ImageMetadataDto } from '../phips-photoblog-common/model/image-metadata-dto';
import { ImageMetadataInput } from '../phips-photoblog-common/model/image-metadata-input';
import { HttpClient } from '@angular/common/http';
import { PhipsPhotoblogAdminModule } from './phips-photoblog-admin.module';
import { map } from 'rxjs/operators';
import { CommonImageService } from '../phips-photoblog-common/service/common-image.service';

@Injectable({
  providedIn: PhipsPhotoblogAdminModule
})
export class AdminImageService {

  constructor(private http: HttpClient) {
  }

  updateMetadata(input: ImageMetadataInput): Observable<ImageMetadataDto> {
    return this.http.post<ImageMetadataDto>('admin/image-metadata', input).pipe(
      map(CommonImageService.transformDto));
  }
}
