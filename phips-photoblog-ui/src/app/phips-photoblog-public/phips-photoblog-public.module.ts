import { NgModule } from '@angular/core';
import { PhipsPhotoblogCommonModule } from '../phips-photoblog-common/phips-photoblog-common.module';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { ImpressumComponent } from './impressum/impressum.component';
import { DatenschutzComponent } from './datenschutz/datenschutz.component';
import { BlogEntryComponent } from './blog-entry/blog-entry.component';

@NgModule({
  imports: [
    PhipsPhotoblogCommonModule
  ],
  declarations: [LoginComponent, HomeComponent, PageNotFoundComponent, AboutComponent, ContactComponent, ImpressumComponent, DatenschutzComponent, BlogEntryComponent]
})
export class PhipsPhotoblogPublicModule { }
