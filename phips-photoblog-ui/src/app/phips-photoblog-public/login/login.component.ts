import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../phips-photoblog-common/service/auth.service';
import { Router } from '@angular/router';
import { LoggingService } from '../../phips-photoblog-common/service/logging.service';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public fg: FormGroup = null;

  public errorMessage: string = null;

  /**
   * we need this to prevent the login component from
   * printing 'login attempt was not successfully' when we logout somewhere
   * in the application
   */
  private isLogin: boolean = false;

  private logger: LoggingService;

  constructor(private fb: FormBuilder,
              private auth: AuthService,
              private router: Router) {
    this.logger = LoggingService.getLogger(LoginComponent.name);
  }

  ngOnInit() {
    this.fg = this.fb.group({
      'username': ['', [Validators.required]],
      'password': ['', [Validators.required]],
    });

    this.auth.authenticationChanges$.pipe(map(x => !!x)).subscribe(isAuthenticated => {
      if (isAuthenticated) {
          this.router.navigateByUrl('/home');
      }

      if (this.isLogin) {
        this.isLogin = false;
        if (isAuthenticated) {
          this.logger.info('LoginComponent: user logged in successfully');
        } else {
          this.logger.info('LoginComponent: user login attempt was not successfully');
          this.errorMessage = 'Failed to login! Try again.';
        }
      }
    });
  }

  loginHandler(): void {
    // and then wait for the observable (see nginit)
    this.isLogin = true;
    this.auth.login(this.getUsername(), this.getPassword());
  }

  getUsername(): string {
    return this.fg.get('username').value;
  }

  getPassword(): string {
    return this.fg.get('password').value;
  }

}
