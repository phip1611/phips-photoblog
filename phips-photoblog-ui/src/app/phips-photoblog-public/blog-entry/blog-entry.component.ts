import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../../phips-photoblog-common/service/auth.service';
import { ImageMetadataDto } from '../../phips-photoblog-common/model/image-metadata-dto';
import { AdminImageService } from '../../phips-photoblog-admin/admin-image.service';
import { LoggingService } from '../../phips-photoblog-common/service/logging.service';
import { ImageMetadataInput } from '../../phips-photoblog-common/model/image-metadata-input';

@Component({
  selector: 'app-blog-entry',
  templateUrl: './blog-entry.component.html'
})
export class BlogEntryComponent implements OnInit {

  @Input()
  public image: ImageMetadataDto = null;

  public editMode: boolean = false;

  // for template driven form
  public newDescription: string = '';
  public newTitle: string = '';

  private logger: LoggingService;

  constructor(public authService: AuthService,
              private adminImageService: AdminImageService) {
    this.logger = LoggingService.getLogger(BlogEntryComponent.name);
  }

  ngOnInit() {
    this.newDescription = this.image.description;
    this.newTitle = this.image.name;
  }

  saveEditHandler(): void {
    let input: ImageMetadataInput = {
      id: this.image.id,
      name: this.newTitle,
      description: this.newDescription
    };

    this.adminImageService.updateMetadata(input).subscribe(dto => {
      this.logger.debug('dto.id=' + dto.id);
      this.editMode = false;
      this.image = dto;
    }, error => {
      this.logger.error(error);
    });
  }

  abortEditHandler(): void {
    this.editMode = false;
    this.newDescription = this.image.description;
    this.newTitle = this.image.name;
  }

}
