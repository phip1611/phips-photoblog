import { Component, OnInit } from '@angular/core';
import { CommonImageService } from '../../phips-photoblog-common/service/common-image.service';
import { ImageMetadataDto } from '../../phips-photoblog-common/model/image-metadata-dto';
import { Page } from '../../phips-photoblog-common/model/page';
import { AuthService } from '../../phips-photoblog-common/service/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  public page: Page<ImageMetadataDto> = null;

  constructor(private commonImageService: CommonImageService,
              public authService: AuthService) { }

  ngOnInit() {
    this.commonImageService.getImages().subscribe(page => this.page = page);
  }

}
