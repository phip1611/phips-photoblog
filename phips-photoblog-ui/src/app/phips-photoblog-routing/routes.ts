import { Routes } from '@angular/router';
import { HomeComponent } from '../phips-photoblog-public/home/home.component';
import { LoginComponent } from '../phips-photoblog-public/login/login.component';
import { ContactComponent } from '../phips-photoblog-public/contact/contact.component';
import { AboutComponent } from '../phips-photoblog-public/about/about.component';
import { AdminHomeComponent } from '../phips-photoblog-admin/admin-home/admin-home.component';
import { AuthGuard } from '../phips-photoblog-common/auth.guard';
import { PageNotFoundComponent } from '../phips-photoblog-public/page-not-found/page-not-found.component';
import { ImpressumComponent } from '../phips-photoblog-public/impressum/impressum.component';
import { DatenschutzComponent } from '../phips-photoblog-public/datenschutz/datenschutz.component';

export const ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'about',
    component: AboutComponent,
  },
  {
    path: 'contact',
    component: ContactComponent,
  },
  {
    path: 'impressum',
    component: ImpressumComponent,
  },
  {
    path: 'datenschutz',
    component: DatenschutzComponent,
  },

  {
    path: 'admin/home',
    component: AdminHomeComponent,
    canActivate: [AuthGuard]
  },

  {
    path: '**',
    component: PageNotFoundComponent,
  }
];
