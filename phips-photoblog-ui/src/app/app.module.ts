import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PhipsPhotoblogRoutingModule } from './phips-photoblog-routing/phips-photoblog-routing.module';
import { PhipsPhotoblogAdminModule } from './phips-photoblog-admin/phips-photoblog-admin.module';
import { PhipsPhotoblogPublicModule } from './phips-photoblog-public/phips-photoblog-public.module';
import { PhipsPhotoblogCommonModule } from './phips-photoblog-common/phips-photoblog-common.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    PhipsPhotoblogRoutingModule,
    PhipsPhotoblogAdminModule,
    PhipsPhotoblogPublicModule,
    PhipsPhotoblogCommonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
