import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UrlReplaceInterceptor } from './interceptor/url-replace.interceptor';
import { AuthService } from './service/auth.service';
import { AuthTokenInvalidateInterceptor } from './interceptor/auth-token-invalidate-interceptor.service';
import { AuthTokenInjectInterceptor } from './interceptor/auth-token-inject-interceptor.service';
import { AuthGuard } from './auth.guard';
import { CommonImageService } from './service/common-image.service';
import { CacheService } from './service/cache.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [],
  providers: [
    AuthGuard,

    {
      provide: HTTP_INTERCEPTORS,
      useClass: UrlReplaceInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthTokenInjectInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthTokenInvalidateInterceptor,
      multi: true,
    },

    // entity services
    CommonImageService
  ],
  exports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PhipsPhotoblogCommonModule { }
