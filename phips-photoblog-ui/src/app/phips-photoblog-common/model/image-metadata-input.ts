export interface ImageMetadataInput {
  id: string,
  name: string,
  description: string
}
