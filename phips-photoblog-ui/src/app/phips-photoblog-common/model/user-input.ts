export interface UserInput {
  id: string;
  username: string;
  name: string;
  familyName: string;
  roles: string[];
  password: string;
}
