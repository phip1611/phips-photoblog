export interface UserDto {
  id: string;
  username: string;
  name: string;
  familyName: string;
  roles: string[];
}
