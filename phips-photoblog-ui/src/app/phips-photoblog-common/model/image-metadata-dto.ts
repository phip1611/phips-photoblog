export interface ImageMetadataDto {
  id: string,
  name: string,
  description: string,
  originalFilename: string,
  creationTime: string | Date;
  lastModifiedTime: string | Date;
  size: number;
  url: string;
}
