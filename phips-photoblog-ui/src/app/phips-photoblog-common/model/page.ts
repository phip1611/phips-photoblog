export interface Page<T> {
  content: T[]; // die Elemente
  totalPages: number;
  totalElements: number;
  size: number;
  first: boolean;
  last: boolean;
  number: number; // aktuelle Seitennummer
  numberOfElements: number; // Einträge auf aktueller Seite
}
