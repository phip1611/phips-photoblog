import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { Level, LoggingService } from '../service/logging.service';

/**
 * Injects the token into requests to the server.
 */
@Injectable()
export class AuthTokenInjectInterceptor implements HttpInterceptor {

  private logger: LoggingService;

  constructor(private authService: AuthService) {
    this.logger = LoggingService.getLogger(AuthTokenInjectInterceptor.name);
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // apply only to requests to the server this is running on,
    // e.q. localhost or my-domain.tld
    if (request.url.includes(location.hostname) && this.authService.token) {
      // 1) for /login we don't want the token because we are using
      // basic auth there (although it would not harm)
      // 2) means we have a token that is (not yet) null
      request = request.clone({
        headers: request.headers
          .append('X-Token', this.authService.token)
          // This Header prevents Spring from responsind with a "www-authenticate: basic"-Header
          // otherwise we could get a browser popup to login when the frontend e.g. tries
          // to login with an invalid token to the /login endpoint
          .append('X-Requested-With', 'XMLHttpRequest')
      });
      this.logger.log(Level.DEBUG, 'X-Token injected into request to: ' + request.url);
    }
    return next.handle(request);
  }

}
