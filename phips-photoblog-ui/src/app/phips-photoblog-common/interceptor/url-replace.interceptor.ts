import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoggingService } from '../service/logging.service';

/**
 * Sorgt in produktiven Umgebungen dafür, dass aus "$host" "api.$host" wird.
 * Dort wird der Service erreichbar sein. Im Dev-Modus werden Requests
 * von :4200 an :8080 umgeleitet.
 */
@Injectable()
export class UrlReplaceInterceptor implements HttpInterceptor {

  private logger: LoggingService;

  constructor() {
    this.logger = LoggingService.getLogger(UrlReplaceInterceptor.name)
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // if we are in ng serve mode: all outgoing http requests (via httpclient) are
    // meant to go to :8080 (our service)
    const url = UrlReplaceInterceptor.normalizeUrl(request);
    this.logger.debug(`normalized url to: '${url}'`);

    if (window.location.hostname === 'localhost' && window.location.port === '4200') {
      this.logger.debug('angular in local dev mode: switched api-target port 4200 to 8080');
      request = request.clone({
        url: url.replace(window.location.host, window.location.hostname + ":8080")
      });
    } else {
      request = request.clone({
        // skip two first characters from url; are '//' because we
        // normalized the url earlier
        url: '//' + 'api.' + url.substr(2)
      });
      this.logger.debug('replaced "' + url + '" w/ "' + request.url+'"');
    }

    return next.handle(request);
  }

  public static normalizeUrl(request: HttpRequest<any>): string {
    let url = '//' + window.location.host; // browser will expand '//' to protocol
    url += request.url.startsWith('/') ? request.url : ('/' + request.url);
    return url;
  }
}
