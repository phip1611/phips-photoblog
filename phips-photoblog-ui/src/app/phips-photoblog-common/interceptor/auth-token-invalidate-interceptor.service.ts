import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { noop, Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';
import { tap } from 'rxjs/operators';
import { LoggingService } from '../service/logging.service';

/**
 * Invalidates the auth token if a invalid response was made
 * to a secured endpoint.
 */
@Injectable()
export class AuthTokenInvalidateInterceptor implements HttpInterceptor {

  private logger: LoggingService;

  constructor(private authService: AuthService) {
    this.logger = LoggingService.getLogger(AuthTokenInvalidateInterceptor.name);
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // apply only to requests to the server this is running on,
    // e.q. localhost or my-domain.tld
    if (request.url.includes(location.hostname)) {
      return next.handle(request).pipe(
        tap(() => noop(), httpErrorRes => {
          if (httpErrorRes instanceof HttpErrorResponse
            && !request.url.includes('/login') // important so that we can properly login without
            // having logout() triggered (which redirects us to /home from /login
            && (httpErrorRes.status == 403 || httpErrorRes.status === 401)) {
            this.logger.debug('HttpResponse caught, Not Authorized Request! Invalidating local token.');
            this.authService.logout();
          }
        })
      );
    } else {
      return next.handle(request);
    }
  }

}
