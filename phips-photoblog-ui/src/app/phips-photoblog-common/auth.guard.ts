import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './service/auth.service';
import { Observable } from 'rxjs';
import { LoggingService } from './service/logging.service';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {

  private logger: LoggingService;

  constructor(private authService: AuthService,
              private router: Router) {
    this.logger = LoggingService.getLogger(AuthGuard.name);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
      return this.authService.authenticationChanges$.pipe(
        map(user => !!user),
        tap(authenticated => {
          if (!authenticated) {
            this.router.navigateByUrl('/login');
          }
        })
      );
  }
}
