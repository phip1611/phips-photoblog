import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { LoggingService } from './logging.service';
import { Router } from '@angular/router';
import { UserDto } from '../model/user-dto';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private static readonly LS_KEY = 'TOKEN';

  private _authenticationChanges$: Subject<UserDto> = new ReplaySubject(1);

  // the token that every authenticated request will have
  private _token: string = null;

  // Der aktuelle User: wie das Observable, nur synchron
  private _user: UserDto;

  private logger: LoggingService;

  constructor(private http: HttpClient,
              private router: Router) {
    this.logger = LoggingService.getLogger(AuthService.name);
  }

  get user(): UserDto {
    return this._user;
  }

  get token(): string {
    if (!this._token) {
      this.logger.warn('Trying to get token but it\'s null');
    }
    return this._token;
  }

  // if user is not logged in the token will be null
  get isLoggedIn(): boolean {
    return !!this._user;
  }

  get authenticationChanges$(): Observable<UserDto> {
    return this._authenticationChanges$.asObservable();
  }

  private static getBasicAuthHeader(username: string, password: string): HttpHeaders {
    const headerName = 'Authorization';
    const headerValue = 'Basic ' + btoa(username + ':' + password);
    return new HttpHeaders().append(headerName, headerValue);
  }

  /**
   * Logs in a user though basic auth and gets a AuthTokenDto back.
   *
   * @param username Username
   * @param password Password
   */
  login(username: string, password: string): void {
    this.http.get<AuthToken>('/login', {headers: AuthService.getBasicAuthHeader(username, password)})
      .pipe(map(tok => {
        // String to Date
        tok.creationDate          = new Date(tok.creationDate);
        tok.lastUseDate           = new Date(tok.lastUseDate);
        tok.lastSuccessfulUseDate = new Date(tok.lastSuccessfulUseDate);
        tok.invalidationDate      = new Date(tok.invalidationDate);
        return tok;
      }))
      .subscribe((token: AuthToken) => {
          this.logger.info('successfully logged in as user: ' + username);
          this._token = token.id;
          localStorage.setItem('TOKEN', this._token);
          this._authenticationChanges$.next(token.user);
          this._user = token.user;
        },
        err => {
          this.logger.warn('unsuccessful login attempt by user: ' + username);
          this.logger.error(err);
        });
  }

  // this can not be called in the constructor because otherwise
  // there are hundreds of hundreds of calls (cyclic thing)..
  // see AppComponent.ngOnInit
  testAuthenticationOnBootstrap() {
    this.logger.debug('testAuthenticationOnBootstrap invoked');
    const lsToken = localStorage.getItem(AuthService.LS_KEY);
    if (lsToken) {
      this._token = localStorage.getItem(AuthService.LS_KEY);
      this.testAuthenticationTokenAndGetUser();
    }
  }

  testAuthenticationTokenAndGetUser(): void {
    this.logger.debug('check if token is valid');
    this.http.get<AuthToken>('/login').subscribe((token: AuthToken) => {
      this.logger.debug('stored token is valid! Authenticated!');
      this._authenticationChanges$.next(token.user);
      this._user = token.user;
    }, () => {
      this.logger.debug('stored token is invalid! not authenticated!');
      this._authenticationChanges$.next(null);
      this._user = null;
      this._token = null;
      localStorage.removeItem(AuthService.LS_KEY);
    });
  }

  logout() {
    this.logger.info('User logged out, redirecting to /home');
    localStorage.removeItem(AuthService.LS_KEY);
    // this.token = null; not necessary, the subscription on
    // authenticationChanges$ already does that on a null value!
    this._authenticationChanges$.next(null);
    this._user = null;
    this.router.navigateByUrl('/home'); // also do redirect, don't rely on the auth guard
    // since it can happen that auth guard thinks we are logged in and just in this moment
    // we get logged out
  }
}

export interface AuthToken {
  id: string;
  user: UserDto;
  creationDate: Date;
  lastUseDate: Date;
  lastSuccessfulUseDate: Date;
  invalidationDate: Date;
  useCount: number;
}
