import { HttpClient } from '@angular/common/http';
import { LoggingService } from './logging.service';
import { Injectable } from '@angular/core';
import { PageRequest } from '../model/page-request';
import { ImageMetadataDto } from '../model/image-metadata-dto';
import { Page } from '../model/page';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable()
export class CommonImageService {

  private static LOGGER: LoggingService = LoggingService.getLogger(CommonImageService.name);

  constructor(private http: HttpClient) {
  }

  getImages(pageRequest?: PageRequest): Observable<Page<ImageMetadataDto>> {
    // todo implement pageRequest
    return this.http.get<Page<ImageMetadataDto>>('/public/images')
      .pipe(
        tap(() => {CommonImageService.LOGGER.debug('got page from server');}),
        map(CommonImageService.transformDtoPage)
      );
  }

  /**
   * Takes a page and transforms all ImageMetaDtos as the definition in {@link #transformDto} says.
   *
   * @param page Page
   */
  public static transformDtoPage(page: Page<ImageMetadataDto>): Page<ImageMetadataDto> {
    CommonImageService.LOGGER.debug('transforming page content');
    page.content = CommonImageService.transformDtos(page.content);
    return page;
  }

  /**
   * Takes a list of {@link ImageMetadataDto} and transforms them as the definition in {@link #transformDto} says.
   *
   * @param dtos
   */
  public static transformDtos(dtos: ImageMetadataDto[]): ImageMetadataDto[] {
    CommonImageService.LOGGER.debug('[1/3] transforming timestrings to JS-Dates');
    CommonImageService.LOGGER.debug('[2/3] make sure all url\'s start with "/"');
    CommonImageService.LOGGER.debug('[3/3] make sure all urls points to :8080 in dev mode');
    return dtos.map(CommonImageService.transformDtoNoDebugLog);
  }

  /**
   * Takes a {@link ImageMetadataDto} and changes the timestamps (String) to J
   *
   * @param dto ImageMetadataDto
   * @param noDebugLog Suppresses Debug Output.
   */
  public static transformDto(dto: ImageMetadataDto): ImageMetadataDto {
    CommonImageService.LOGGER.debug('[1/3] transforming timestrings to JS-Dates');
    CommonImageService.LOGGER.debug('[2/3] make sure url\'s start with "/"');
    CommonImageService.LOGGER.debug('[3/3] make sure url points to :8080 in dev mode');
    return CommonImageService.transformDtoNoDebugLog(dto);
  }

  /**
    Is used only for {@link #transformDtos} and get's invoked by {@link #transformDto}.
   */
  private static transformDtoNoDebugLog(dto: ImageMetadataDto): ImageMetadataDto {
    // 1/3
    dto.creationTime = new Date(dto.creationTime);
    dto.lastModifiedTime = new Date(dto.lastModifiedTime);

    // 2/3
    dto.url = dto.url.startsWith('/') ? dto.url : '/'+dto.url;

    // 3/3
    if (window.location.port === '4200') {
      dto.url = '//localhost:8080' + dto.url;
    } else {
      dto.url = '//api.' + location.host + dto.url;
    }

    return dto;
  }
}
