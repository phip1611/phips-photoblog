import { Injectable } from '@angular/core';
import { LoggingService } from './logging.service';

/**
 * This service can be used to cache fetched data from servers when e.g.
 * switching from Master To Detail view. This is useful when you don't
 * use Master-Slave-Views but Child-Routes. Then you can temporarily store
 * the subject of that action.
 */
@Injectable({
  providedIn: 'root'
})
export class CacheService {

  private cache: Map<String, any> = new Map<String, any>();

  private logger: LoggingService;

  constructor() {
    this.logger = LoggingService.getLogger(CacheService.name);
  }

  public clear(): void {
    this.logger.debug('Cleared Cache');
    this.cache.clear();
  }

  public put(key: String, value: any): void {
    this.logger.debug('put value with key="'+key+'"');
    this.cache.set(key, value);
  }

  public delete(key: String): void {
    this.logger.debug('deleted value with key="'+key+'"');
    this.cache.delete(key);
  }

  public get(key: String): any {
    this.logger.debug('get value for key="'+key+'"');
    return this.cache.get(key);
  }

}
