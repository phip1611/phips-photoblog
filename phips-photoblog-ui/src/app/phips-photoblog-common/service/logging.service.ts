import { env } from '../../../environments/env';

/**
 * Logging-Service mit verschiedenen Logging-Stufen.
 */
export class LoggingService {

  // private static readonly LOCAL_STORAGE_KEY = 'logger-level';

  /**
   * Aktuelles Level, bis zu dem Ausgaben ausgegeben werden sollen.
   */
  private static _level: Level;

  /**
   * Name des Loggers. In der Regel der Service oder die Komponente, die ihn benutzt.
   */
  private readonly name: string;

  private constructor(name) {
    this.name = name;
  }

  public static getLogger(name: string): LoggingService {
    if (!LoggingService._level) {
      this.setLevel(env.prod ? Level.INFO : Level.ERROR);
    }

    return new LoggingService(name);
  }

  public static setLevel(level: Level|any): void {
    if (this.isValidLevel(level)) {
      this._level = level;
    }
  }

  /**
   * Setzt das Level statisch und global für alle Logging-Service Instanzen.
   * @param level Level
   */
  public setLevel(level: Level|any): void {
    LoggingService.setLevel(level);
  }

  public log(level: Level, obj: any) {
    if (level >= LoggingService.level) {
      console.log('%c' + LoggingService.getPrefix(level) + ` %c[${this.name}]: %c` + obj,
        'color: ' + LoggingService.getPrefixColor(level),
        'color: #bbb',
        'color: #000'
      );
    }
  }

  public error(obj: any) {
    this.log(Level.ERROR, obj);
  }

  public debug(obj: any) {
    this.log(Level.DEBUG, obj);
  }

  public warn(obj: any) {
    this.log(Level.WARN, obj);
  }

  public info(obj: any) {
    this.log(Level.INFO, obj);
  }

  public static get level(): Level {
    return this._level;
  }

  private static getPrefix(level: Level): string {
    return Level[level];
  }

  private static isValidLevel(x: any): boolean {
    return !!Level[x];
  }

  private static getTimestamp(): string {
    let datestr = '';
    let date = new Date();
    return date.toLocaleDateString()+'T'+date.toLocaleTimeString()
  }

  private static getPrefixColor(level: Level) {
    switch (level) {
      case Level.ERROR: {
        return '#ff3f27';
      }
      case Level.WARN: {
        return '#ffaf3d';
      }
      case Level.DEBUG: {
        return '#2172ff';
      }
      case Level.INFO: {
        return '#b0c2ee';
      }
      default: {
        return null;
      }
    }
  }
}

export enum Level {
  ERROR,
  WARN,
  DEBUG,
  INFO,
  NONE
}
