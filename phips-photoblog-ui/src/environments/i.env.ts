/**
 * This Interface describes which properties an environment must have.
 */
export interface IEnv {
    prod: boolean;
}
