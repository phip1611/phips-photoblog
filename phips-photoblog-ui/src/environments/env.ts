import { IEnv } from './i.env';

/**
 * Default Environment. You have to import env properties from this file because this is the file
 * where the NG CLI magic happens and environment files get's replaced!
 */
export const env: IEnv = {
    prod: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
