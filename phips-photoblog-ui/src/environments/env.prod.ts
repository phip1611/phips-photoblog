import { IEnv } from './i.env';

/**
 * Replaces the default env in production build/mode.
 */
export const env: IEnv = {
  prod: true
};
