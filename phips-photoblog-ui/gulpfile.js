let gulp = require('gulp');
let gulp_gzip = require('gulp-gzip');
let gulp_brotli = require('gulp-brotli');

let filePattern = "dist/**/*.{js,css,html,txt,ico,svg,xml,json}";

/*
  Dateien statisch vorkomprimieren, damit der nginx (oder apache)
  sie ausliefern kann.
 */

gulp.task('static_brotli', () => {
  return gulp.src(filePattern)
    .pipe(gulp_brotli.compress({
      // Empirische Tests (je 10 Versuche) mit Juniorzeit.de haben ergeben, dass die Stufe 11 für bis
      // zu 150ms zusätzliche Dekompressionstime benötigt bei gerade einmal 6% weniger Daten.
      // Daher Stufe 9 statt 11!

      // Ladezeit 10x mit Stufe 11:  756,1ms (Durchschnitt)
      // Ladezeit 10x mit Stufe  9:  569,5ms (Durchschnitt)
      quality: 9
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('static_gzip', () => {
  return gulp.src(filePattern)
    .pipe(gulp_gzip({gzipOptions: {
        level: 8,
        extension: 'gz'
    }}))
    .pipe(gulp.dest('dist'));
});

gulp.task('post_prod-build', gulp.parallel('static_brotli', 'static_gzip'));
