# https://spin.atomicobject.com/2017/08/24/start-stop-bash-background-process/
# kill all childs if we kill this script
trap "exit" INT TERM
trap "kill 0" EXIT

cd phips-photoblog-service
./build_raspi.sh &
cd ..
cd phips-photoblog-ui
./build_raspi.sh &
cd ..

# wait for all background processes
wait %1 %2

echo "ui and service build; now building docker images"

cd phips-photoblog-ui-docker-standalone
./build_raspi.sh &
cd ..
cd phips-photoblog-ui-docker-slave
./build_raspi.sh &
cd ..

wait %1 %2

echo "build done"
