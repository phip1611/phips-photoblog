# Phips Photoblog

A photo blog with Spring Backend and Angular Frontend 

# Build + deploy to docker
`$ maven clean install [-P docker]` (on X86)
`$ ./build_raspi.sh` (on ARM)

Make sure your ~/.m2/settings.xml has an auth entry for docker.io

```
  <servers>
    <server>
      <id>docker.io</id>
      <username>phip1611</username>
      <password>%TOKEN%</password> <!-- Docker-Hub Auth Token -->
    </server>
  </servers>
```




# Licences
## YAUAA
License: https://github.com/nielsbasjes/yauaa/blob/master/LICENSE